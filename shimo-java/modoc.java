import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Function;

class Benchmark {
    public static void main(String[] args) {
        Class_827 obj = new Class_827();
        Class_870 obj1 = new Class_870();
        System.out.println(obj.getGulByAttributeKey("hideGridLine"));
    }
}

class GlobalClass {
    static String method_125(Object obj) {
        return obj.getClass().getName();
    }

    static boolean hasOwnProperty(Object obj, String propertyName) {
        Class<?> clazz = obj.getClass();
        try {
            Field field = clazz.getDeclaredField(propertyName);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }
}

class Class_963 {
    ArrayList ops = new ArrayList<>();
    boolean stringified;

    class PushResult {
        int PUSHED = 0;
        int MERGED = 1;
        int REORDERED = 2;
        Map<Integer, String> map = Map.of(
                PUSHED, "PUSHED",
                MERGED, "MERGED",
                REORDERED, "REORDERED"
        );
    }

    PushResult d = new PushResult();

    Class_963(ArrayList ops) {
        if (ops == null) {
            ops = new ArrayList<>();
        }
        this.stringified = false;
        this.ops = ops;
    }

    int push(Class_793 h) {
        if (this.stringified) {
            throw new IllegalArgumentException("Cannot push new operators to a stringified operatorList");
        }
        int c = this.ops.size();
        if (c == 0) {
            this.ops.add(h);
            return this.d.PUSHED;
        }
        int f = d.PUSHED;
        Class_793 i = (Class_793) this.ops.get(c - 1);
        if (i.action.equals("remove") && h.action.equals("insert")) {
            c -= 1;
            i = (Class_793) this.ops.get(c - 1);
            f = d.REORDERED;
            if (i == null) {
                this.ops.add(0, h);
                return f;
            }
        }
        Class_793 a = i.merge(h);
        if (a != null) {
            this.ops.set(c - 1, a);
            f = d.MERGED;
        } else {
            if (c == this.ops.size()) {
                this.ops.add(h);
            } else {
                this.ops.add(c, h);
            }
        }
        return f;
    }

    String stringify(Boolean h) {
        if (h == null) {
            h = false;
        }
        this.stringified = true;
        String f = "";
        int c = this.ops.size();
        for (int i = 0; i < c; i++) {
            Class_793 a = (Class_793) this.ops.get(i);
            if (!h || i < c - 1 || !(a == null)) {
                f += a.stringify();
            }
        }
        return f;
    }

}

//433
class Pool {
    String __MODOC_NAME__;
    Map<String, Object[]> cache;
    String packedData;
    ArrayList<String> array;
    boolean enableCache;
    boolean enableNullIgnore;
    Map<String, Object> rawStringIgnoreAttrs;

    Class_511 y = new Class_511();
    Class_827 h = new Class_827();
    Class_472 c = new Class_472();
    boolean f = false;
    boolean i = false;
    boolean a = false;
    Map<String, Object> e = new HashMap<>();
    Map<String, String> map = new HashMap<>();

    Pool(Object t) {
        this.__MODOC_NAME__ = "Pool";
        if (t == null) {
            this.array = new ArrayList<>();
        } else {
            if (t instanceof String) {
                try {
                    JSONArray arr = new JSONArray((String) t);
                    this.array = new ArrayList<>();
                    for (int i = 0; i < arr.length(); i++) {
                        this.array.add(arr.getString(i));
                    }
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                this.packedData = (String) t;
            } else {
                this.array = ((Pool) t).array;
                this.packedData = ((Pool) t).packedData;
            }
        }
    }

    Pool(ArrayList<String> array, String packedData) {
        this.array = array;
        this.packedData = packedData;
    }

    boolean isPool(Object t) {
        if (t instanceof Object && t != null) {
            Class<?> clazz = t.getClass();
            try {
                Field field = clazz.getDeclaredField("__MODOC_NAME__");
                field.setAccessible(true);
                Object obj = field.get(t);
                return obj.equals("Pool");
            } catch (NoSuchFieldException ex) {
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }
        return false;
    }

    void setIPoolConfig(Pool t) {
        if (t == null) {
            return;
        }
        f = !!(t.enableCache);
        i = !!(t.enableNullIgnore);
        a = t.rawStringIgnoreAttrs != null;
        if (t.rawStringIgnoreAttrs != null) {
            e = t.rawStringIgnoreAttrs;
        }
    }

    boolean isSubsetOf(Pool t) {
        if (this == t) {
            return true;
        }
        int n = this.array.size();
        if (n == 0) {
            return true;
        }
        if (n > t.array.size()) {
            return false;
        }
        for (int s = 0; s < n; s++) {
            if (!this.array.get(s).equals(t.array.get(s))) {
                return false;
            }
        }
        return true;
    }

    Map<String, String> getMap() {
        for (int t = 0; t < this.array.size(); t++) {
            this.map.put(this.array.get(t), String.valueOf(t));
        }
        return this.map;
    }

    String packAttributes(Map<String, String> t) {
        if (t == null) {
            return "";
        }
        String[] n = (String[]) t.keySet().toArray();
        int s = n.length;
        if (s == 0) {
            return "";
        }
        ArrayList<String> u = new ArrayList<>();
        ArrayList<String> l = new ArrayList<>();
        for (int v = 0; v < s; v++) {
            String w = n[v];
            String b = t.get(w);
            if (b instanceof Object && b != null) {
                List<Object[]> k = c.objectToKeyPaths(b, "." + w, null);
                List<Object[]> D = k;
                for (int A = 0; A < D.size(); A++) {
                    Object[] P = D.get(A);
                    String O = this.getAddress((String) P[0], (String) P[1]);
                    if (O.charAt(0) == ' ') {
                        u.add(O);
                    } else {
                        l.add(O);
                    }
                }
            } else {
                String O = this.getAddress(w, b);
                if (O.charAt(0) == ' ') {
                    u.add(O);
                } else {
                    l.add(O);
                }
            }
        }
        Collections.sort(l);
        Collections.sort(u);
        return String.join("", l) + String.join("", u);
    }

    String getAddress(String t, String n) {
        String s = h.combinedAttributeWithValue(t, n);
        String u = h.getGulByCombinedKeyValue(s);
        if (u instanceof String) {
            return u;
        }
        Map<String, String> l = getMap();
        if (!l.containsKey(s)) {
            this.packedData = null;
            int v = this.array.size();
            l.put(s, String.valueOf(v));
            this.array.add(s);
        }
        return (" " + l.get(s));
    }

    Map<String, String> unpackAttributes(String t, Boolean n) {
        if (n == null) {
            n = true;
        }
        Map<String, String> s = new HashMap<>();
        int uu = 0;
        for (int u = 0; u < t.length() && t.substring(u, u + 1).equals(" "); u++) {
            String[] v = h.getKeyValueByGul(t.charAt(u));
            String w = v[0];
            String b = v[1];
            if (n && w.charAt(0) == '.') {
                c.keyPathToObject(s, w, b);
            } else {
                s.put(w, b);
                uu += 1;
            }
        }
        if (uu < t.length() - 1) {
            String[] k = t.substring(uu + 1).split(" ");
            uu++;
            String[] D = k;
            for (int A = 0; A < D.length; A++) {
                String P = D[A];
                Object[] O;
                if (f) {
                    if (this.cache.containsKey(P)) {
                        O = this.cache.get(P);
                    } else {
                        Object[] result = h.parseCombinedAttributeValue(this.array.get(Integer.valueOf(P)));
                        this.cache.put(P, result);
                        O = result;
                    }
                } else {
                    O = h.parseCombinedAttributeValue(this.array.get(Integer.valueOf(P)));
                }
                String w = (String) O[0];
                String b = (String) O[1];
                if (n && w.charAt(0) == '.') {
                    c.keyPathToObject(s, w, b);
                } else if (!i || b != null) {
                    s.put(w, b);
                    if (e.get(w) != null) {
                        String E = s.containsKey("rawString") ? s.get("rawString") : t;
                        int j = P.length() + 1;
                        int anonymous = uu - 1;
                        s.put("rawString", (E.substring(0, anonymous) + E.substring(anonymous + j)));
                    }

                }
                uu += P.length() + 1;
            }
        }
        if (a && !s.containsKey("rawString")) {
            s.put("rawString", t);
        }
        return s;
    }

    String stringify() {
        if (this.packedData != null && !(this.packedData instanceof String)) {
            if (this.array.size() > 0) {
                JSONArray jsonArray = new JSONArray();
                for (String item : this.array) {
                    jsonArray.put(item);
                }
                this.packedData = jsonArray.toString();
            } else {
                this.packedData = "";
            }
        }
        return this.packedData;
    }

    public Pool clone() {
        return new Pool(new ArrayList<>(this.array), this.packedData);
    }
}

class Class_511 {
    String TYPE_PROPERTY = "__MODOC_NAME__";
}

class Class_827 {
    Map<String, String> keyToGul = Map.ofEntries(
            Map.entry("color", "0"),
            Map.entry("background", "1"),
            Map.entry("font-size", "2"),
            Map.entry("width", "3"),
            Map.entry("height", "4"),
            Map.entry("rowspan", "5"),
            Map.entry("colspan", "6"),
            Map.entry("align", "7"),
            Map.entry("bold", "8"),
            Map.entry("italic", "9"),
            Map.entry("underline", "a"),
            Map.entry("vertical", "b"),
            Map.entry("strike", "c"),
            Map.entry("wrap", "d"),
            Map.entry("fixedRowsTop", "e"),
            Map.entry("fixedColumnsLeft", "f"),
            Map.entry("link", "g"),
            Map.entry("format", "h"),
            Map.entry("formula", "i"),
            Map.entry("filter", "j"),
            Map.entry("border-top", "k"),
            Map.entry("border-right", "l"),
            Map.entry("border-bottom", "m"),
            Map.entry("border-left", "n"),
            Map.entry("line", "o"),
            Map.entry("guid", "p"),
            Map.entry("author", "q"),
            Map.entry("size", "r"),
            Map.entry("list", "s"),
            Map.entry("layout", "t"),
            Map.entry("margin", "u"),
            Map.entry("font", "v"),
            Map.entry("header", "w"),
            Map.entry("indent", "x"),
            Map.entry("comment", "y"),
            Map.entry("comment-block", "z"),
            Map.entry("code-block", "A"),
            Map.entry("name", "B"),
            Map.entry("order", "C"),
            Map.entry("autoFit", "D"),
            Map.entry("userID", "E"),
            Map.entry("nextIndex", "F"),
            Map.entry("id", "G"),
            Map.entry("colVisible", "H"),
            Map.entry("rowVisible", "I"),
            Map.entry("autoFormatter", "J"),
            Map.entry("copyIndex", "K"),
            Map.entry("manualAlign", "L"),
            Map.entry("lock", "M"),
            Map.entry("left", "N"),
            Map.entry("top", "O")
    );
    String[] d = {"color", "background", "font-size", "width", "height", "rowspan", "colspan", "align", "bold", "italic", "underline", "vertical", "strike", "wrap", "fixedRowsTop", "fixedColumnsLeft", "link", "format", "formula", "filter", "border-top", "border-right", "border-bottom", "border-left", "line", "guid", "author", "size", "list", "layout", "margin", "font", "header", "indent", "comment", "comment-block", "code-block", "name", "order", "autoFit", "userID", "nextIndex", "id", "colVisible", "rowVisible", "autoFormatter", "copyIndex", "manualAlign", "lock", "left", "top", "cellValue"};

    String getAttributeKeyByGul(char e) {
        int o = (int) e;
        String r;
        if (o >= 48 && o <= 57) {
            r = d[o - 48];
        } else if (o >= 65 && o <= 90) {
            r = d[o - 29];
        } else if (o >= 97 && o <= 122) {
            r = d[o - 87];
        } else {
            throw new IllegalArgumentException("Unknown gul \"" + e + "\"");
        }
        return r;
    }

    String getGulByAttributeKey(String e) {
        String o = this.keyToGul.get(e);
        if (o != null) {
            return o;
        }
        String r = Integer.toString(e.length(), 36);
        int t = r.length();
        if (t > 2) {
            throw new IllegalArgumentException("Key length of attribute should <= 2: " + r);
        }
        String n = t == 1 ? "$" : "%";
        return "" + n + r + e;
    }

    String combinedAttributeWithValue(String e, Object o) {
        String r = getGulByAttributeKey(e);
        return (o instanceof String ? r + ":" + o : o instanceof Number ? r + "*" + String.valueOf(o) : o instanceof Boolean ? r + ((Boolean) o ? "1" : "0") : r);
    }

    Map<String, String> keyValueToGul = Map.ofEntries(
            Map.entry("81", "!"),
            Map.entry("2*12", "\""),
            Map.entry("2*11", "#"),
            Map.entry("2*10", "$"),
            Map.entry("2*9", "%"),
            Map.entry("2*8", "&"),
            Map.entry("6*2", "'"),
            Map.entry("6*3", "("),
            Map.entry("6*4", ")"),
            Map.entry("5*2", "*"),
            Map.entry("5*3", "+"),
            Map.entry("5*4", ","),
            Map.entry("0:#ff0000", "-"),
            Map.entry("0:#ffffff", "."),
            Map.entry("b:bottom", "/"),
            Map.entry("h:normal", "0"),
            Map.entry("1", "1"),
            Map.entry("1:#ffffff", "2"),
            Map.entry("1:#ff0000", "3"),
            Map.entry("1:#ffff00", "4"),
            Map.entry("1:#00ffff", "5"),
            Map.entry("7:left", "6"),
            Map.entry("7:center", "7"),
            Map.entry("7:right", "8"),
            Map.entry("1*4", "9"),
            Map.entry("d:text-wrap", ":"),
            Map.entry("d:text-no-wrap", ";"),
            Map.entry("h:YYYY/MM/DD", "<"),
            Map.entry("l:[\"\",1]", "="),
            Map.entry("n:[\"\",1]", ">"),
            Map.entry("k:[\"\",1]", "?"),
            Map.entry("m:[\"\",1]", "@"),
            Map.entry("l:[\"#2b2b2b\",1]", "A"),
            Map.entry("n:[\"#2b2b2b\",1]", "B"),
            Map.entry("k:[\"#2b2b2b\",1]", "C"),
            Map.entry("m:[\"#2b2b2b\",1]", "D")
    );
    String[][] c = {{"bold", "true"}, {"font-size", "12"}, {"font-size", "11"}, {"font-size", "10"}, {"font-size", "9"}, {"font-size", "8"}, {"colspan", "2"}, {"colspan", "3"}, {"colspan", "4"}, {"rowspan", "2"}, {"rowspan", "3"}, {"rowspan", "4"}, {"color", "#ff0000"}, {"color", "#ffffff"}, {"vertical", "bottom"}, {"format", "normal"}, {"background", null}, {"background", "#ffffff"}, {"background", "#ff0000"}, {"background", "#ffff00"}, {"background", "#00ffff"}, {"align", "left"}, {"align", "center"}, {"align", "right"}, {"background", "4"}, {"wrap", "text-wrap"}, {"wrap", "text-no-wrap"}, {"format", "YYYY/MM/DD"}, {"border-right", "[\"\",1]"}, {"border-left", "[\"\",1]"}, {"border-top", "[\"\",1]"}, {"border-bottom", "[\"\",1]"}, {"border-right", "[\"#2b2b2b\",1]"}, {"border-left", "[\"#2b2b2b\",1]"}, {"border-top", "[\"#2b2b2b\",1]"}, {"border-bottom", "[\"#2b2b2b\",1]"}};

    String getGulByCombinedKeyValue(String e) {
        return this.keyValueToGul.get(e);
    }

    String[] getKeyValueByGul(char e) {
        int o = (int) e - 33;
        return c[o];
    }

    Object[] parseCombinedAttributeValue(String e) {
        String o;
        if (e.charAt(0) == '$') {
            int r = Integer.parseInt(String.valueOf(e.charAt(1)), 36);
            o = e.substring(2, 2 + r);
            e = e.substring(1 + r);
        } else if (e.charAt(0) == '%') {
            int r = Integer.parseInt(e.substring(1, 3), 36);
            o = e.substring(3, 3 + r);
            e = e.substring(2 + r);
        } else {
            o = getAttributeKeyByGul(e.charAt(0));
        }
        if (e.length() == 1) {
            return new Object[]{o, null};
        }
        char t = e.charAt(1);
        if (t == '1') {
            return new Object[]{o, true};
        }
        if (t == '0') {
            return new Object[]{o, false};
        }
        String n = e.substring(2);
        return new Object[]{o, (t == ':' ? n : Double.parseDouble(n))};
    }
}

class Class_472 {
    List<Object[]> objectToKeyPaths(Object c, String f, List<Object[]> i) {
        Class<?> clazz = c.getClass();
        // 获取对象的所有字段（包括私有字段）
        Field[] a = clazz.getDeclaredFields();
        if (i == null) {
            i = new ArrayList<>();
        }
        for (Field r : a) {
            r.setAccessible(true); // 设置字段可访问（包括私有字段）
            try {
                Object t = r.get(c);
                String n = f + "." + r.getName();
                if (t instanceof Object && t != null) {
                    objectToKeyPaths(t, n, i);
                } else {
                    Object[] obj = {n, t};
                    i.add(obj);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return i;
    }

    //Todo待优化
    void keyPathToObject(Map<String, String> c, String f, String i) {
        String[] a = f.split("\\.");
        int e = a.length;
        for (int o = 1; o < e - 1; o++) {
            String r = a[o];
            Object t = c.get(r);
            if (t instanceof Map && t != null) {
                c = (Map<String, String>) t;
            } else {
                c.put(r, "");
                c = (Map<String, String>) t;
            }
        }

        String n = a[e - 1];
        if (i != null && (c.get(n) == null || c.get(n) == null)) {
            c.put(n, i);
        }
    }

}

class Class_793 {
    String action;
    Object data;
    static Map<String, Object> dataHandlers;
    String packedAttributes;
    Pool pool;
    String dataType;
    String packedData;
    Class_870 y = new Class_870();
    Class_292 h = new Class_292();
    String c = "0:";
    int length = 0;
    Class_793() {

    }

    Class_793(String action, Object data, String packedAttributes, Pool pool) {
        new Class_793(action, data, pool, packedAttributes);
    }

    Class_793(String action, Object data, Pool pool, String packedAttributes) {
        this.action = action;
        this.data = data;
        this.pool = pool;
        this.packedAttributes = packedAttributes;
    }

    Class_793(Class_793Sub e, String o) {
        this.action = e.action;
        Map<String, Object> r = e.data;
        if (r == null) {
            throw new IllegalArgumentException("\"data\" is required for an Operator");
        }
        if (r instanceof Object) {
            String[] result = (String[]) r.keySet().toArray();
            String t = result[0];
            Object n = r.get(t);
            if (n instanceof String) {
                this.data = r;
                this.dataType = t;
            } else {
                Object s = Class_793.dataHandlers.get(t);
                if (s == null) {
                    throw new IllegalArgumentException("Unsupported nested data type \"" + t + "\"");
                }
                //Todo 待补充
                String u = s.toString();
                if (t.equals("delta") && u.equals(c)) {
                    this.data = 1;
                    this.dataType = "number";
                } else {
                    Map<String, String> l = new HashMap<>();
                    l.put(t, u);
                    this.data = l;
                    this.dataType = t;
                }
            }

        } else {
            this.data = r;
            this.dataType = r.getClass().getName();
        }
        this.packedAttributes = e.packedAttributes;
        this.pool = e.pool;
        this.packedData = o;

    }

    boolean isOperator(Object e) {
        try {
            Method method = e.getClass().getDeclaredMethod("getAttributes");
            return true;
        } catch (NoSuchMethodException ex) {
            return false;
        }
    }

    static void registerDataHandler(String e, Object o) {
        Class_793.dataHandlers.put(e, o);
    }

    Object getUnresolvedAttributes(Object e) {
        try {
            Method method = e.getClass().getDeclaredMethod("getUnresolvedAttributes");
            return method.invoke(e);
        } catch (NoSuchMethodException ex) {
            try {
                Method method = e.getClass().getDeclaredMethod("getAttributes");
                try {
                    return method.invoke(e);
                } catch (IllegalAccessException | InvocationTargetException ignored) {
                }
            } catch (NoSuchMethodException ignored) {

            }
        } catch (InvocationTargetException ex) {
            throw new RuntimeException(ex);
        } catch (IllegalAccessException ignored) {
        }
        return null;
    }

    Object getUnresolvedAttributes() {
        if (this.pool == null && this.hasAttributes()) {
            return this.pool.unpackAttributes(this.packedAttributes, false);
        } else {
            return new Object();
        }
    }

    int getLength() {
        if (this.data instanceof String) {
            return ((String) this.data).length();
        } else if (this.data instanceof Number) {
            return ((Number) this.data).intValue();
        } else {
            return 1;
        }
    }

    boolean isisEmpty() {
        return this.action.equals("retain") && (this.packedAttributes.equals("") || this.packedAttributes == null) && (this.data instanceof Number);
    }

    Object getParsedData() {
        Object e = Class_793.dataHandlers.get(this.dataType);
        if (e == null) {
            throw new Error("Unknown handler for " + this.dataType);
        }
//        return e.parse(this.getCustomData());
        //Todo 待补充
        return new Object();
    }

    boolean hasAttributes() {
        return this.packedAttributes != null && this.packedAttributes.equals("");
    }

    Object getAttributes() {
        if (this.pool != null && this.hasAttributes()) {
            return this.pool.unpackAttributes(this.packedAttributes, null);
        } else {
            return new Object();
        }
    }

    String stringify() {
        if (this.packedData == null) {
            Object e = this.data;
            String o = null;

            switch (this.action) {
                case "insert":
                    o = "!";
                    break;

                case "retain":
                    o = "@";
                    break;

                default:
                    o = "#";
            }
            if (e instanceof Number) {
                o += Long.toString(((Number) e).longValue(), 36) + "#";
            } else if (e instanceof String) {
                String str = (String) e;
                o += Long.toString(str.length(), 36) + "!" + str;
            } else {
                String r = this.dataType;
                if (r == null) {
                    throw new Error("ASSERT: dataType === undefined");
                }
                String t = y.objectKeyToInitial.get(r);
                if (t == null) {
                    throw new Error("Unsupport object data type: \"" + r + "\"");
                }
                String n = "" + t + ((Map<String, String>) e).get(r);
            }
            this.packedData = o;
        }
        String s = (this.packedAttributes == null) ? this.packedData : this.packedData + this.packedAttributes;
        return Long.toString(s.length(), 36) + s;
    }

    Class_793 merge(Class_793 e) {
        if (!this.action.equals(e.action)) {
            return null;
        }
        if (this.data instanceof Object && (GlobalClass.method_125(this.data).equals(GlobalClass.method_125(this.data)))) {
            return null;
        }
        boolean r = this.hasAttributes();
        if (r != e.hasAttributes()) {
            return null;
        }

        if (r) {
            if (this.pool.equals(e.pool)) {
                if (!this.packedAttributes.equals(e.packedAttributes)) {
                    return null;
                } else if (!i(this.getUnresolvedAttributes(), e.getUnresolvedAttributes())) {
                    return null;
                }
            }
        }
        return new Class_793(this.action, (this.data.toString() + e.data.toString()), this.pool, this.packedAttributes);
    }

    Object getCustomData() {
        switch (GlobalClass.method_125(this.data)) {
            case "java.lang.String":
            case "java.lang.Integer":
            case "java.lang.Long":
            case "java.lang.Double":
                throw new Error("Cannot get custom data from a simple operator");
            default:
                try {
                    Field field = this.data.getClass().getDeclaredField(this.dataType);
                    return field.get(this.data);
                } catch (NoSuchFieldException | IllegalAccessException ignored) {
                    return null;
                }

        }
    }

    Class_793 compose(Class_793 e, Map<String, Object> o) {
        if (this.action.equals("remove")) {
            throw new Error("Removing operator cannot compose other operators");
        }
        if (!e.action.equals("retain")) {
            throw new Error("Only retaining operator can be composed");
        }
        Object r;
        boolean t = false;
        Object n;
        Map<String, Object> w;
        if (e.hasAttributes()) {
            if (this.pool == null) {
                throw new Error("this.pool is undefined when composing");
            }
            Object s = this.getUnresolvedAttributes();
            if (o.get("checkParaEnd") != null && GlobalClass.hasOwnProperty(s, "paraEnd") && (this.dataType instanceof String)) {
                throw new Error("ParaEnd overwrite string in document");
            }
            t = GlobalClass.hasOwnProperty(s, "cellValue");
            Object u = h.merge(this.getUnresolvedAttributes(), (Map<String, String>) s, ((boolean) o.get("isDocument")));
            n = this.pool.packAttributes((Map<String, String>) u);
        } else {
            n = this.packedAttributes;
        }
        if (t) {
            if (e.data instanceof Number) {
                r = e.data;
            } else {
                r = 1;
            }
        } else if (this.data instanceof Number) {
            r = e.data;
        } else if (e.data instanceof Number) {
            r = this.data;
        } else {
            if (this.dataType == null) {
                throw new Error("Missing \"dataType\" when composing " + this.data + " with " + e.data);
            }
            if (!this.dataType.equals(e.dataType)) {
                throw new Error("Expect " + e.dataType + " to equal " + this.dataType + " when composing " + this.data + " with " + e.data);
            }
            Object l = Class_793.dataHandlers.get(this.dataType);
            if (l == null) {
                throw new Error("Unknown handler for " + this.dataType);
            }
//            Object v = .compose(this.getCustomData(), e.getCustomData(), o);
//todo 待修改
            Object v = new Object();
            if (this.dataType.equals("delta") && v.equals(c)) {
                r = 1;
            } else {
                w = new HashMap<>();
                w.put(this.dataType, v);
                r = w;
            }
        }
        return new Class_793(this.action, e, this.pool, (String) n);
    }

    Class_793 transform(Class_793 e, Boolean o) {
        if (o == null) {
            o = false;
        }
        if (!this.action.equals("retain")) {
            throw new Error("Only retaining operator can transform other operators");
        }
        if (e.action.equals("retain")) {
            throw new Error("Only retaining operator can be transformed");
        }
        Object r;
        Map<String, Object> l;
        if (this.dataType.equals("number") || e.dataType.equals("number")) {
            r = e.data;
        } else {
            if (this.dataType == null) {
                throw new Error("Missing \"dataType\" when transforming " + this.data + " with " + e.data);
            }
            if (this.dataType.equals(e.dataType)) {
                throw new Error("Expect " + e.dataType + " to equal " + this.dataType + " when transforming " + this.data + " with " + e.data);
            }
            Object t = Class_793.dataHandlers.get(this.dataType);
            if (t == null) {
                throw new Error("Unknown handler for " + this.dataType);
            }
//           todo待补充
//            String n = t.transform(this.getCustomData(), e.getCustomData(), o);
            String n = "";
            if (this.dataType.equals("delta") && n.equals(c)) {
                r = 1;
            } else {
                l = new HashMap<>();
                l.put(this.dataType, n);
                r = l;
            }
        }
        Object s;

        if (e.hasAttributes()) {
            Object u = h.transform(this.getUnresolvedAttributes(), this.getUnresolvedAttributes(e), o);
            if (e.pool == null) {
                throw new Error("ASSERT: other.pool === undefined when transforming");
            }
            s = e.pool.packAttributes((Map<String, String>) u);
        } else {
            s = "";
        }
        return new Class_793(this.action, r, e.pool, (String) s);
    }

    Class_793 invert(Class_793 e, Boolean o) {
        if (this.action.equals("insert")) {
            throw new Error("invert should only apply to the \"insert\" action (\" + this.dataType + \")");
        }
        if (e.action.equals("retain")) {
            throw new Error("Only retaining operator can be inverted");
        }
        Object r = null;
        boolean t = false;
        Object n;
        if (e.hasAttributes()) {
            var s = this.getUnresolvedAttributes(e);
            t = GlobalClass.hasOwnProperty(s, "cellValue");
            Object u = h.invert(this.getUnresolvedAttributes(), s);
            if (e.pool == null) {
                throw new Error("ASSERT: other.pool === undefined when inverting");
            }
            n = e.pool.packAttributes((Map<String, String>) u);
        } else {
            n = "";
        }

        if (t) {
            r = this.data;
        } else if (o != null) {
            r = o;
        } else if (e.data instanceof Number) {
            r = e.data;
        } else if (this.data instanceof Number && !e.dataType.equals("delta")) {
            r = this.data;
        } else {
            if (this.dataType == null) {
                throw new Error("Missing \"dataType\" when inverting " + this.data + " with " + e.data);
            }

            if (this.dataType.equals("number") && e.dataType.equals("delta")) {
                Class_793 l = (Class_793) Class_793.dataHandlers.get("delta");
                Map<String, Object> map = new HashMap<String, Object>();
                //todo 待修改
                map.put("delta", new Object());
                r = map;
            } else {
                if (this.dataType.equals(e.dataType)) {
                    throw new Error("Expect " + e.dataType + " to equal " + this.dataType + " when inverting " + this.data + " with " + e.data);
                }
                Object l = Class_793.dataHandlers.get(this.dataType);
                if (l == null) {
                    throw new Error("Unknown handler for " + this.dataType);
                }
                //todo 待修改
                var v = new Object();
//                var v = l.invert(this.getCustomData(), e.getCustomData());
                if (this.dataType.equals("delta") && v.equals(c)) {
                    Map<String, Object> w = new HashMap<>();
                    w.put(this.dataType, v);
                    r = w;
                }
            }
        }

        return new Class_793("retain", r, e.pool, (String) n);
    }

    Map<Object, Object> toJSON() {
        Map<Object, Object> e = new HashMap<>();
        e.put("action", this.action);
        if (this.hasAttributes()) {
            e.put("attributes", this.getAttributes());
        }
        if (this.dataType.equals("number") || this.dataType.equals("string")) {
            e.put(data, this.data);
        } else {
            Object o = Class_793.dataHandlers.get(this.dataType);
            Object r = this.getCustomData();
            Map<String, Object> t = new HashMap<>();
            //todo 待补充
//            e.data = (t = {}, t[this.dataType] = o ? o.toJSON(r) : r, t);
            t.put(this.dataType, o);
            e.put("data", t);

        }
        return e;
    }

    String explain(String e) {
        if (e == null) {
            e = "";
        }
        int o = this.stringify().length();
        String r = e + "[" + this.dataType + "(" + this.action + ")] #" + (o - String.valueOf(o).length()) + ":\n";
        if (this.dataType.equals("number") || this.dataType.equals("string")) {
            r += e + this.data;
        } else {
            Object t = Class_793.dataHandlers.get(this.dataType),
                    n = this.getCustomData();
//            todo 待补充
//            r += t != null? ((Class_793)t).explain(e + "  ", n) : n;
            r += t != null ? "" : n;
        }
        return r + "\n" + e + "PackedAttributes:" + this.packedAttributes + "\n";
    }


    boolean i(Object a, Object e) {
        if (a.equals(e)) return true;
        Field[] o = a.getClass().getDeclaredFields();
        int r = o.length;
        if (r != a.getClass().getDeclaredFields().length) {
            return false;
        }
        for (int t = 0; t < r; t++) {
            String name = o[t].getName();
            try {
                Field field = e.getClass().getDeclaredField(name);
                if (!o[t].get(a).equals(field.get(e))) {
                    return false;
                }
            } catch (NoSuchFieldException | IllegalAccessException ex) {
                return false;
            }
        }
        return true;
    }


}

class Class_3 {
    String ops;
    Pool pool;
    Pool targetPool;
    int cursor;
    int offset;
    OperatorConstructor operatorConstructor;
    NextData nextData;
    Boolean isCompatible;
    Class_870 g = new Class_870();
    Class_963 y = new Class_963(null);

    Class_3(String f, Pool i, Pool a, OperatorConstructor e) {
        this.ops = f;
        this.pool = i;
        this.targetPool = a;
        this.operatorConstructor = e;
        this.cursor = 0;
        this.offset = 0;
    }

    Boolean isPoolCompatible() {
        if (this.isCompatible == null) {
            this.isCompatible = this.pool.isSubsetOf(this.targetPool);
        }
        return this.isCompatible;
    }

    boolean hasNext() {
        return this.cursor < this.ops.length();
    }

    String remainingStrs(Class_963 f) {
        for (; this.hasNext(); ) {
            Class_793 i = this.next(null, null);
            int a = f.push(i);

            if (a != y.d.REORDERED) {
                var e = this.cursor;
                this.cursor = this.ops.length();
                this.nextData = null;
                return f.stringify(null) + this.ops.substring(e);
            }
        }

        return f.stringify(true);
    }

    Object[] skip(Integer f) {
        if (f == null) {
            f = 1 / 0;
        }
        NextData i = this.peekNext();
        if (i == null) {
            throw new Error("No more operators");
        }
        int a = this.offset;
        int e;
        if (f + a > i.dataLength) {
            e = i.dataLength - a;
            this.nextData = null;
            this.cursor = i.opIndex + i.opLength;
            this.offset = 0;
        } else {
            e = f;
            this.offset += e;
        }
        return new Object[]{e, a, i};
    }


    int peekLength() {
        var f = this.peekNext();
        return f == null ? 1 / 0 : f.dataLength - this.offset;
    }
    String peekAction() {
        NextData f = this.peekNext();
        return f == null ? "retain" : f.action;
    }

    Class_793 next(Integer f, String i) {
        if (f == null) {
            f = 1 / 0;
        }
        Object[] a = this.skip(f);
        int e = (int) a[0];
        int o = (int) a[1];
        NextData r = (NextData) a[2];
        Object t;

        switch (r.dataType) {
            case "number":
                t = e;
                break;

            case "string":
                t = this.ops.substring(r.dataIndex + 1 + o, r.dataIndex + 1 + o + e);
                break;

            default:
                String n = this.ops.substring(r.dataIndex + 1 + o, r.dataIndex + 1 + o + 1);
                String s = this.ops.substring(r.dataIndex + 1 + 1, r.dataIndex + 1 + r.actualLength);
                Map<String, String> b = new HashMap<>();
                b.put(g.initialToObjectKey.get(n), s);
                t = b;
                break;
        }

        String u = this.ops.substring(r.dataIndex + 1 + r.actualLength, r.opIndex + r.opLength);
        Pool l = this.targetPool;
        boolean v = !u.equals("") && !this.isPoolCompatible();
        String w;
        if (o == 0 && e == r.dataLength && !v) {
            w = this.ops.substring(r.opIndex, r.dataIndex + 1 + r.actualLength);
        }
        if (v) {
            u = l.packAttributes(this.pool.unpackAttributes(u, null));
        }
        return this.operatorConstructor.call((i != null ? i : r.action), t, u, l);
    }

    NextData peekNext() {
        if (!this.hasNext()) {
            return null;
        }

        if (this.nextData == null) {
            String f = null;
            int i = 0;
            i = this.cursor + 1;
            int a = this.ops.length();
            for (; i < a; ) {
                switch (this.ops.charAt(i)) {
                    case '!':
                        f = "insert";
                        break;

                    case '@':
                        f = "retain";
                        break;

                    case '#':
                        f = "remove";
                        break;
                }

                if (f != null) break;
                i += 1;
            }

            int e = i;
            int o = Integer.parseInt(this.ops.substring(this.cursor, e), 36);
            i = e + 1;
            String r = null;
            for (; i < a; ) {
                switch (this.ops.charAt(i)) {
                    case '!':
                        r = "string";
                        break;

                    case '#':
                        r = "number";
                        break;

                    case '@':
                        r = "object";
                        break;
                }

                if (r != null) break;
                i += 1;
            }

            int t = i;
            int n = Integer.parseInt(this.ops.substring(e + 1, t), 36);
            int s = r.equals("number") ? 0 : n;
            int u = r.equals("object") ? 1 : n;
            this.nextData = new NextData(f, t, u, e, o, r, s);
        }

        return this.nextData;
    }
}

class NextData {
    String action;
    int dataIndex;
    int dataLength;
    int opIndex;
    int opLength;
    String dataType;
    int actualLength;

    NextData(String action, int dataIndex, int dataLength, int opIndex, int opLength, String dataType, int actualLength) {
        this.action = action;
        this.dataIndex = dataIndex;
        this.dataLength = dataLength;
        this.opIndex = opIndex;
        this.opLength = opLength;
        this.dataType = dataType;
        this.actualLength = actualLength;
    }
}
class Class_116 {
    String possibleDeltaInputToString(Object g, Object y) {
        if(y == null) {
            return "";
        }
        //todo 待补充
        if (y instanceof List<?>) {
            if (((List<?>)y).size() == 0) {
                return "";
            }

            Class_877 h = new Class_877(((List<?>)y).toArray(), null);
            return h.isEmpty() ? "" : h.stringify();
        } else if (y instanceof Array) {
            if(((Object[])y).length == 0) {
                return  "";
            }
            Class_877 h = new Class_877(y, null);
            return h.isEmpty() ? "" : h.stringify();
        }
        return ((Class_877)y).isEmpty() ? "" : ((Class_877)y).stringify();
    }
}

class Class_793Sub {
    String action;
    Map<String, Object> data;
    String packedAttributes;
    Pool pool;
}

class Class_870 {
    Map<String, String> initialToObjectKey = Map.ofEntries(
            Map.entry("A", "attachment"),
            Map.entry("C", "cell"),
            Map.entry("I", "image"),
            Map.entry("D", "delta"),
            Map.entry("T", "table"),
            Map.entry("P", "presentation"),
            Map.entry("S", "slide"),
            Map.entry("M", "mention"),
            Map.entry("N", "inline-break"),
            Map.entry("V", "divide"),
            Map.entry("U", "upload-placeholder-attachment"),
            Map.entry("G", "gallery"),
            Map.entry("B", "gallery-block"),
            Map.entry("Z", "mindmap"),
            Map.entry("O", "object"),
            Map.entry("F", "form")
    );
    Map<String, String> objectKeyToInitial = new HashMap<>();

    Class_870() {
        for (Map.Entry<String, String> entry : initialToObjectKey.entrySet()) {
            objectKeyToInitial.put(entry.getValue(), entry.getKey());
        }
    }

}

class Class_292 {
    Object merge(Object h, Map<String, String> c, Boolean f) {
        Class<?> clazz = h.getClass();
        Field[] i = clazz.getDeclaredFields();
        if (f && i.length == 0) {
            return c;
        }
        Set<String> a = c.keySet();
        for (String r : a) {
            try {
                Field field = clazz.getDeclaredField("name");
                field.set(h, c.get(r));
                if (!f && field.get(h) == null) {
                    field.set(h, null);
                }
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return h;
    }

    Object transform(Object h, Object c, boolean f) {
        if (f) {
            return c;
        }
        Field[] i = c.getClass().getDeclaredFields();
        if (i.length == 0) {
            return c;
        }
        Field[] a = h.getClass().getDeclaredFields();
        if (a.length == 0) {
            return c;
        }
        if (a.length > i.length) {
            Field[] o = i;
            for (int e = 0; e < o.length; e++) {
                String r = o[e].getName();
                try {
                    Field field = c.getClass().getDeclaredField(r);
                    field.set(h, null);
                } catch (NoSuchFieldException ex) {
                    throw new RuntimeException(ex);
                } catch (IllegalAccessException ex) {
                    throw new RuntimeException(ex);
                }
            }
        } else {
            Field[] n = a;
            for (int t = 0; t < n.length; t++) {
                String s = n[t].getName();
                try {
                    Field field = c.getClass().getDeclaredField(s);
                    field.set(c, null);
                } catch (NoSuchFieldException ex) {
                    throw new RuntimeException(ex);
                } catch (IllegalAccessException ex) {
                    throw new RuntimeException(ex);
                }
            }
        }
        return c;
    }

    Object invert(Object h, Object c) {
        Field[] f = c.getClass().getDeclaredFields();
        if (f.length == 0) {
            return c;
        }
        Field[] i = h.getClass().getDeclaredFields();
        if (i.length == 0) {
            Field[] e = f;
            for (int a = 0; a < f.length; a++) {
                String o = e[a].getName();
                try {
                    Field field = c.getClass().getDeclaredField(o);
                    field.set(c, null);
                } catch (NoSuchFieldException ex) {
                    throw new RuntimeException(ex);
                } catch (IllegalAccessException ex) {
                    throw new RuntimeException(ex);
                }
            }
        } else {
            Field[] t = f;
            for (int r = 0; r < t.length; r++) {
                String o = t[r].getName();
                try {
                    Object n = h.getClass().getDeclaredField(o).get(h);
                    Field field = c.getClass().getDeclaredField(o);
                    field.set(c, (n == null ? null : n));
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (NoSuchFieldException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return c;
    }

}

class Class_297 {
    boolean isDelta(Object h) {
        try {
            Field field = h.getClass().getDeclaredField("__MODOC_NAME__");
            return field.get(h).equals("Delta");
        } catch (NoSuchFieldException | IllegalAccessException ignored) {
        }
        return false;
    }
}

class Class_877 {
    Class_963 h = new Class_963(null);
    Pool c;
    Class_511 f = new Class_511();
    Class_793 i = new Class_793();
    Class_3 a;
    Class_297 e = new Class_297();
    String __MODOC_NAME__;
    String ops;
    Pool pool;
    String action;
    Object data;
    Object attributes;

    Class_877(Object t, Object n) {
        Class_963 s = h;
        this.__MODOC_NAME__ = "Delta";
        if (t instanceof List<?>) {
            ArrayList<Class_793> tList = (ArrayList<Class_793>) t;
            if (!c.isPool(n) || tList.size() == 0) {
                n = c;
            }
            ;
            ArrayList<Class_793> l = tList;
            for (int u = 0; u < l.size(); u++) {
                Class_793 v = l.get(u);
//                    todo 待补充
//                    Object w = ((Pool)n).packAttributes(v.attributes);
                String w = "";
                s.push(new Class_793(v.action, v.data, (Pool) n, w));
            }

            this.ops = s.stringify(true);
            this.pool = (Pool) n;
        } else if (n == null) {
            int b = ((String) t).indexOf(":");
            if (b <= 0) {
                throw new Error("Invalid delta: " + t);
            }
            int k = Integer.parseInt(((String) t).substring(0, b), 36);
            if (Double.isNaN(k)) {
                throw new Error("Invalid delta: " + t);
            }
            int A = b + 1 + k;
            this.ops = ((String) t).substring(b + 1, A);
            this.pool = k == 0 || ((String) t).length() == A ? new Pool(null) : new Pool(((String) t).substring(b + k + 1));
        } else {
            this.ops = ((String) t);
            this.pool = ((String) t).equals("") ? new Pool(null) : (Pool) n;
        }
    }

    Map<String, Object> assign(Map<String, Object>... sources) {
        Map<String, Object> target = new HashMap<>();
        for (Map<String, Object> source : sources) {
            for (Map.Entry<String, Object> entry : source.entrySet()) {
                target.put(entry.getKey(), entry.getValue());
            }
        }
        return target;
    }
    int getLength() {
        Class_3 n = this.getIter(null);
        int s = 0;
        for (; n.hasNext(); ) {
            s += n.peekLength();
            n.skip(null);
        }
        return s;
    }
    boolean isEmpty() {
        return this.ops.length() == 0;
    }
    Pool getPoolCopy() {
        return this.pool.clone();
    }
    Class_877 map(Function<Class_793, Class_877> t) {
        ArrayList<Object> n = new ArrayList<>();
        Class_3 s = this.getIter(null);
        for (; s.hasNext(); ) {
            n.add(t.apply(s.next(null, null)));
        }
        return new Class_877(n, null);
    }
    Class_3 getIter(PoolClone t) {
        PoolClone n = t == null ? new PoolClone() : t;
        Pool s = n.targetPool,
                u = n.sourcePool;
        return new Class_3(this.ops, u != null ? u : this.pool, s != null ? s : this.pool, Class_793::new);
    }
    Class_877 compose(Class_877 t, Object n) {
        compose s = new compose();
        if (n != null) {
            if (n instanceof Boolean) {
                s.isDocument = (boolean) n;
            } else if (n instanceof Object) {
                s = (compose) n;
            }
        }
        Pool u = this.getPoolCopy();
        Class_963 l = new Class_963(null);
        Class_3 v = this.getIter(new PoolClone(u, u));

        if (t instanceof List<?>) {
            t = new Class_877(t, u);
        }
        Class_3 w = ((Class_877) t).getIter(new PoolClone(u));
        while (w != null) {
            boolean b = v.hasNext();
            boolean k = w.hasNext();
            if (!b && !k) {
                break;
            }
            if (!k) return new Class_877(v.remainingStrs(l), u);
            if (!b && w.isPoolCompatible()) {
                return new Class_877(w.remainingStrs(l), u);
            }
            if (w.peekAction().equals("insert")) {
                l.push(w.next(null, null));
            } else if (v.peekAction().equals("remove")) {
                l.push(v.next(null, null));
            } else {
                int A = Math.min(v.peekLength(), w.peekLength());
                Class_793 D = b ? v.next(A, null) : null,
                        P = w.next(A, null);
                if (P.action.equals("retain")) {
                    if (D != null) {
                        boolean O = s.isDocument == null ? D.action.equals("insert") : s.isDocument;
                        Map<String, Object> y = new HashMap<>();
                        y.put("isDocument", O);
                        l.push(D.compose(P, y));
                    } else {
                        l.push(P);
                    }
                } else {
                    if (D != null || D.action.equals("retain")) {
                        l.push(P);
                    }
                }
            }
        }

        return new Class_877(l.stringify(true), u);
    }
    String explain(String t) {
        if (t == null) {
            t = "";
        }
        String n = t + "Delta #" + this.ops.length() + ":\n";
        Class_3 s = this.getIter(null);
        for (; s.hasNext(); ) {
            n += s.next(null, null).explain(t + "  ");
        }
        return "" + n + t + "Pool: " + this.pool.stringify() + "\n";
    }
    String stringify() {
        return this.isEmpty() ? "0:" : Integer.toString(this.ops.length(), 36) + ":" + this.ops + this.pool.stringify();
    }
    //方法未调用
    Class_793[] toJSON() {
        Class_793[] t = new Class_793[0];
        return t;
    }
    Class_877 validateTable() {
        Class_3 n = this.getIter(null);
        for (;n.hasNext();) {
            Map<String,Object> t = new HashMap<>();
            Class_793 s = n.next(null, null);

            if (s.dataType.equals("table")) {
                Map<String,String>attr = (Map<String, String>) s.getAttributes();
                Object u =  attr.get("name");
                if (!(u instanceof String)) continue;
                var l = "$$" + u + "$$";
                if (t.containsKey(l)) throw new Error("Duplicated table name found: " + u);
                t.put(l,true);
            }
        }

        return this;
    }
    Class_877 invert(Object t, Object n) {
        return new Class_877(null,null);
    }
    Class_877 transform(Object t, Object n) {
        return new Class_877(null,null);
    }

}

class Class_800 {

    Class_116 y = new Class_116();
    Class_776 h = new Class_776();

    String pack(Pack_800 t) {
        Class_877 n = t.rows, s = t.cols;
        String u = y.possibleDeltaInputToString(null, n);
        String l = y.possibleDeltaInputToString(null, s);
        return Integer.toString(u.length(), 36) + "$" + u + l;
    }

    String explain(String t, String n) {
        String s = t + "Rows:\n";
        Class_877[] u = this.parse(n);
        Class_877 l = u[0],
                v = u[1];
        if (l == null) {
            s = s + "[EMPTY]";
        } else {
            s = s + l.explain(t);
        }
        s += t + "Cols:\n";
        if (v == null) {
            s = s + "[EMPTY]";
        } else {
            s = s + v.explain(t);
        }
        return s;
    }

    Class_877[] parse(String t) {
        int n = t.indexOf("$");
        int s = Integer.parseInt(t.substring(0, n), 36);
        int u = n + 1 + s;

        Class_877 l = s == 0 ? null : new Class_877(t.substring(n + 1, u), null);
        Class_877 v = t.length() == u ? null : new Class_877(t.substring(u), null);

        Class_877[] result = new Class_877[2];
        result[0] = l;
        result[1] = v;
        return result;
    }

    String compose(String t, String n, compose s) {
        Class_877[] u = this.parse(t);
        Class_877 l = u[0];
        Class_877 v = u[1];
        Class_877[] w = this.parse(n);
        Class_877 b = w[0];
        Class_877 k = w[1];
        Class_877 A = b == null ? l : l == null ? b : l.compose(b, s);
        Class_877 D = h.reindexInnerDeltas(v, b);
        Class_877 P = k == null ? D : D == null ? k : (Class_877) D.compose(k, s);

        return pack(new Pack_800(A, P));
    }
}

class Class_252 extends Class_134{
    //方法未调用
    @Override
    String pack(Object a) {
        return super.pack(a);
    }
    @Override
    Object[] toJSON() {
        return super.toJSON();
    }
    String explain(Object a, Object i) {
        return "";
    }
}

class Class_696 extends Class_228{
    //方法未调用
    String compose(Object i, Object a, Object e) {
        return "";
    }
    String transform(Object i, Object a, Object e) {
        return "";
    }

    String invert(Object i, Object a) {
        return "";
    }
    Object[] parse(Object a) {
        return new Object[0];
    }
    Object[] toJSON(Object i) {
        return new Object[0];
    }
    String pack(Object i) {
        return "";
    }
    String explain(Object i, Object a) {
        return "";
    }
}

class Class_996 extends Class_228 {
    //方法未调用
    String compose(Object i, Object a, Object e) {
        return "";
    }
    String transform(Object i, Object a, Object e) {
        return "";
    }

    String invert(Object i, Object a) {
        return "";
    }
    Object[] parse(Object a) {
        return new Object[0];
    }
    Object[] toJSON(Object i) {
        return new Object[0];
    }
    String pack(Object i) {
        return "";
    }
    String explain(Object i, Object a) {
        return "";
    }
}

class Class_134 {
    //方法未调用
    String compose(Object a, Object e, Object o) {
        return "";
    }
    String transform(Object a, Object e, Object o) {
        return "";
    }

    String invert(Object a, Object e, Object o) {
        return "";
    }
    Object[] parse(Object a) {
        return new Object[0];
    }
    Object[] toJSON() {
        return new Object[0];
}
    String pack(Object a) {
        return "";
    }
    String explain(Object a, Object e) {
        return "";
    }
}

class Class_113{
    String compose(String a, String e, compose o) {
       return this.parse(a).compose(this.parse(e), o).stringify();
    }
    String transform(String a, String e, compose o) {
        return this.parse(a).transform(this.parse(e), o).stringify();
    }
    Class_877 parse(Object y) {
        return new Class_877(y, null);
    }

    //方法未调用
    String invert(String a, String e) {
        return this.parse(a).invert(this.parse(e), null).stringify();
    }
    void pack(Object a) {
    }
    Class_793[] toJSON(Object a) {
      return this.parse(a).toJSON();
    }
    String explain(String a, String e) {
        return this.parse(e).explain(a);
    }

}

class Class_202 {
   static void registerDataHandlers() {
        Class_793.registerDataHandler("delta", new Class_113());
        Class_793.registerDataHandler("table", new Class_800());
        Class_793.registerDataHandler("presentation", new Class_252());
        Class_793.registerDataHandler("slide", new Class_696());
        Class_793.registerDataHandler("mindmap", new Class_696());
        Class_793.registerDataHandler("object", new Class_996());
        Class_793.registerDataHandler("form", new Class_134());
    }
}

class Class_397 {
    Class_397() {
        Class_202.registerDataHandlers();
    }
}


class Class_776 {
    Class_963 g = new Class_963(null);
    Class_397 y = new Class_397();
    Class_793 h = new Class_793();
    ArrayList<ArrayList<Object>> deltaToIndexModifiers(Class_877 o) {
        Class_3 n = o.getIter(null);
        int t = -1;
        ArrayList<ArrayList<Object>> r = new ArrayList<>();
        for (;n.hasNext();) {
            Class_793 s = n.next(null, null);
            String u = s.action;
            int l = s.length;
            if(t != -1 && r.get(t).get(0).equals(u)) {
                t ++;
                r.set(t,new ArrayList<>(Arrays.asList(u,l)));
            }
        }
        return r;
    }

    Class_877 reindexDelta(Class_877 o, ArrayList<ArrayList<Object>> r) {
        Pool t = o.getPoolCopy();
        Class_963 n  = new Class_963(null);
        Class_3 s = o.getIter(new PoolClone(t,t));
        ArrayList<ArrayList<Object>>u = new ArrayList<>();
        for (int l = 0; l < r.size() && s.hasNext(); l++) {
            ArrayList<?> v = u.get(l) != null ? u.get(l) : r.get(l);
                    String w = (String) v.get(0);
                    int b = (int) v.get(1);

            switch (w) {
                case "insert":
                    n.push(new Class_793("retain",b,"",t));
                    break;

                case "retain":
                {
                    var k = s.peekLength();
                    if(k >= b) { n.push(s.next(b, null));} else {
                        n.push(s.next(null, null));
                        u.set(l,new ArrayList<>(Arrays.asList("retain", b - k)));
                         l -= 1;
                    }
                    break;
                }

                case "remove":
                {
                    var k = s.peekLength();
                    if(k >= b) {
                        s.skip(b);
                    }else {
                        s.skip(null);
                        u.set(l,new ArrayList<>(Arrays.asList("remove", b - k)));
                        l -= 1;
                    }
                    break;
                }
            }
        }

        for (; s.hasNext();) n.push(s.next(null,null));
        return new Class_877(n.stringify(true), t);
    }

    Class_877 reindexInnerDeltas(Class_877 o, Class_877 r) {
        if (o == null || r == null) {
            return o;
        }
        ArrayList<ArrayList<Object>> t = deltaToIndexModifiers(r);
        if (t.size() == 0) {
            return o;
        } else {
            return o.map(n -> {
                Class_877 s = new Class_877(n.action, n.data);
                if (!s.action.equals("remove") && n.hasAttributes()) {
                    s.attributes = n.getAttributes();
                }
                if (n.dataType.equals("delta")) {
                    Class_877 u  = reindexDelta(new Class_877(n.getCustomData(), null), t);
                    s.data = u.isEmpty() ? 1 : Map.of("delta",u.stringify());
                } else {
                    s.data = n.data;

                }
                return s;
            });

        }
    }
    //todo 方法未调用
    void a(Object o, Object r){
    }
    void invertInnerDeltas(Object o, Object r) {

    }
}

class Class_228 {
    DeltaConstructor deltaConstructor;
    OperatorConstructor operatorConstructor;
    Class_228() {
    }
    Class_228(DeltaConstructor deltaConstructor, OperatorConstructor operatorConstructor) {
        this.deltaConstructor = deltaConstructor;
        this.operatorConstructor = operatorConstructor;
    }
    String compose(Object y, Object h, Object c) {
        throw new Error("#compose() is not implemented");
    }
    String transform(Object y, Object h, Object c) {
        throw new Error("#transform() is not implemented");
    }
    String invert(Object y, Object h) {
        throw new Error("#invert() is not implemented");
    }
    Object[] parse(Object y) {
        throw new Error("#parse() is not implemented");
    }
    void pack(Object y, Object h) {
        throw new Error("#pack() is not implemented");
    }
    Object[] toJSON(Object y) {
        throw new Error("#toJSON() is not implemented");
    }
    String explain(Object y, Object h) {
        return "" + y + h;
    }
}

class PoolClone {
    Pool targetPool;
    Pool sourcePool;
    PoolClone(Pool sourcePool,Pool targetPool) {
        this.targetPool = targetPool;
        this.sourcePool = sourcePool;
    }
    PoolClone() {

    }
    PoolClone(Pool targetPool) {
        this.targetPool = targetPool;
    }
}

class Pack_800 {
    Class_877 rows;
    Class_877 cols;

    Pack_800(Class_877 rows, Class_877 cols) {
        this.rows = rows;
        this.cols = cols;
    }
}

class compose {
    Boolean isDocument;
    compose(Boolean isDocument) {
        this.isDocument = isDocument;
    }
    compose() {
    }
}

@FunctionalInterface
interface OperatorConstructor {
    Class_793 call(String action, Object data, String packedAttributes, Pool pool);
}

@FunctionalInterface
interface DeltaConstructor {
    Class_877 call(Object t, Object n);
}