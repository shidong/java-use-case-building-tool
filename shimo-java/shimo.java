import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

class ShiMo {
    static String readCompilerInput(String path) {
        File file = new File(path);
        StringBuilder content = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                content.append(line);
                content.append(System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content.toString();
    }

    static String content = readCompilerInput("./content.txt");
    static String change = readCompilerInput("./change.txt");

    public static void main(String[] args) {
        double startTime = (double) System.nanoTime() / 1_000_000;
        compose(content,change);
        double endTime = (double) System.nanoTime() / 1_000_000;
        System.out.println("shimo: ms = " + (endTime-startTime));
    }

    static Class_877[] compose(String content, String changes) {
        String[] splits = changes.split("\n");
        Class_877 delta = new Class_877(content, null);
        ArrayList<Class_877> result = new ArrayList<>();
        for (String change : splits) {
            result.add(new Class_877(change,null));
        }
        return  result.size() == 0 ? (Class_877[]) result.toArray() : new Class_877[]{delta};
    }

}